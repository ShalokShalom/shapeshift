#!/usr/bin/env fish

function shapeshift
   argparse 'h/help' -- $argv
   if not parameter_is_provided $argv; set -q _flag_help
      echo "Shapeshift lets you change into another root system."
      echo "It is a simple tool that mounts, and chroots a partition of your choice."
      echo ""
      echo "Use it in the following way:"
      set_color green; echo "shapeshift nvme0n1p4"; set_color normal
      echo ""
      echo "Here is a list of the available partitions:"
      echo ""
      lsblk -o NAME,SIZE,MOUNTPOINTS,TYPE | awk '$4 == "part"'
      return 0
end


   # Create the directory if it does not exist
   if not test -d /mnt/$argv
      sudo mkdir "/mnt/$argv"
   end

   # Create and handle the mountpoint
   if not mountpoint -q /mnt/$argv
      sudo mount /dev/$argv /mnt/$argv
   else
      read -p 'set_color green; echo -n "$prompt Existing mount found. Do you want to access it, unmount, or cancel the operation? [a/u/C]: "; set_color normal' -l ask
      switch $ask
      case A a
         echo "Accessing $argv"
      case U u
         cd /mnt/$argv
         sudo umount {proc,sys,dev}
         cd ..
         sudo umount $argv/
         return 0
      case C c ''
         return 1
      end
   end

   # Mount and change root
   cd /mnt/$argv
   sudo mount -t proc proc proc/
   sudo mount -t sysfs sys sys/
   sudo mount -o bind /dev dev/
   sudo cp -L /etc/resolv.conf etc/resolv.conf
   sudo chroot . /bin/bash
end