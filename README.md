# shapeshift

Shapeshift lets you to dynamically change into another root system, 
which is essentially a different operating system environment, or partition on your computer.

The function takes one input argument, which is the name of the partition you want to access (e.g., nvme0n1p4). 

`fish shapeshift.fish nvme0n1p4`

You can also put it into your fish configuration file, and call it with `shapeshift nvme0n1p4`

If you run the function without providing an argument or with the -h or --help flag, 
it will display a help message explaining how to use the function and list the available partitions on your system.

When you are done with your work, exit the chroot with `exit`.
If you like to unmount or reaccess the chroot, just run the command again. 